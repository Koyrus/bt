import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import 'react-select-v1/dist/react-select.css';
import App from "./components/App/App";

window.$ = window.jQuery = require('jquery'); // not sure if you need this at all

ReactDOM.render(<App/>, document.getElementById('banach'));
