import React, {Component, Fragment} from 'react';
import style from './App.scss'
import PlayingTableComponent from "../PlayingTableComponent/PlayingTableComponent";
import FirstPlayerComponent from "../Players/FirstPlayerComponent/FirstPlayerComponent";
import SecondPlayerComponent from "../Players/SecondPlayerComponent/SecondPlayerComponent";

const cards = [
  {id: 1 , value: 'K' , suit: 'clubs'  , valueCount: 4},
  {id: 2 , value: '7' , suit: 'hearts' , valueCount: 0},
  {id: 3 , value: 'Q' , suit: 'diamonds' , valueCount: 3},
  {id: 4 , value: 'J' , suit: 'hearts' , valueCount: 2} ,
  {id: 5 , value: '10' , suit: 'spades' , valueCount: 10},
  {id: 6 , value: '9' , suit: 'hearts' , valueCount: 0},
  {id: 7 , value: 'A' , suit: 'spades' , valueCount: 11},
  {id: 8 , value: 'K' , suit: 'diamonds' , valueCount: 4},
];
const secondPlayerCards = [
  {id: 1 , value: 'Q' , suit: 'clubs'  , valueCount: 3},
  {id: 2 , value: '10' , suit: 'hearts' , valueCount: 0},
  {id: 3 , value: 'Q' , suit: 'diamonds' , valueCount: 3},
  {id: 4 , value: 'J' , suit: 'hearts' , valueCount: 2} ,
  {id: 5 , value: '8' , suit: 'spades' , valueCount: 0},
  {id: 6 , value: '9' , suit: 'hearts' , valueCount: 0},
  {id: 7 , value: 'A' , suit: 'spades' , valueCount: 11},
  {id: 8 , value: 'A' , suit: 'diamonds' , valueCount: 11},
];

class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      firstPlayerCard : [],
      secondPlayerCard : [],
    }
  }

  getFirstPlayerCard = (playerCard) => {
    this.setState({firstPlayerCard : playerCard})
    console.log(playerCard)
  };
  getSecondPlayerCard = (playerCard) => {
    this.setState({secondPlayerCard : playerCard})
  };

    render() {
        return (
            <Fragment>
              <div className={style.tableRow}>
                <div className="container">
                  <div className="row">
                    <div className={`col`}>
                      <FirstPlayerComponent firstPlayer={true} cardArray={secondPlayerCards} getFirstPlayerCard={()=>this.getFirstPlayerCard()}/>
                    </div>
                  </div>
                </div>

                      <PlayingTableComponent/>
                <div className="container">
                    <div className="row">
                      <div className={`col`}>
                        <SecondPlayerComponent firstPlayer={false} cardArray={cards} getSecondPlayerCard={this.getSecondPlayerCard}/>
                      </div>
                  </div>
                </div>
              </div>
            </Fragment>
        );
    }
}

export default App;
