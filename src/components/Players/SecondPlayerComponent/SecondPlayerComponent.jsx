import React, {Component, Fragment} from 'react';
import PlayingDeckComponent from "../../PlayingDeck/PlayingDeckComponent";

class SecondPlayerComponent extends Component{

  constructor(props){
    super(props)
  }

  render(){
    const {firstPlayer , cardArray} = this.props;
    return(
      <Fragment>
        <PlayingDeckComponent firstPlayer={firstPlayer} cardArray={cardArray}/>
      </Fragment>
    )
  }
}
export default SecondPlayerComponent;