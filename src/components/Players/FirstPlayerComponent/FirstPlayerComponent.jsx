import React, {Component, Fragment} from 'react';
import PlayingDeckComponent from "../../PlayingDeck/PlayingDeckComponent";

class FirstPlayerComponent extends Component{

  constructor(props){
    super(props)
  }

  render(){
    return(
      <Fragment>
        <PlayingDeckComponent {...this.props} />
      </Fragment>
    )
  }
}
export default FirstPlayerComponent;