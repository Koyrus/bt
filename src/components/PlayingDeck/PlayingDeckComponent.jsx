import React, {Component, Fragment} from 'react';
import deckStyle from './PlayingDeckComponent.scss'
import crown from '../../styles/images/shapes.svg'
import queens_crown from '../../styles/images/party.svg'
import swords from '../../styles/images/sports.svg'
import clubs from '../../styles/images/clubs.svg'
import diamonds from '../../styles/images/diamonds.svg'
import spades from '../../styles/images/spades.svg'
import hearts from '../../styles/images/hearts.svg'
export default class PlayingDeckComponent extends Component{


  calcPlayerCard(card){
    const {getFirstPlayerCard } = this.props;
    console.log(card)
  }

  playerCards= () => {
    const {firstPlayer , cardArray  } = this.props;
    if(firstPlayer) {
      return (

          <Fragment>
            <div className={`row ${deckStyle.cardsRow}`}>
              {
                cardArray.map(card => {
                return(
                  <div className="col"  key={card.id + 1}>
                    <div className={deckStyle.wrapper} >
                      <div className={deckStyle.front} onClick={()=>this.calcPlayerCard(card)}>
                      </div>
                    </div>
                  </div>

                )
              })}
            </div>
          </Fragment>

      )
    }else {
      return(
        <Fragment>
          <div className={`row ${deckStyle.cardsRow}`}>
          {cardArray.map(card => {
            return(
                <div className="col"  key={card.id + 1}>
                  <div className={deckStyle.wrapper} >
                    <div className={deckStyle.back}>
                      <div className={deckStyle.topPart}>
                        <div className={deckStyle.cardSuitTopLeft}>
                          <img className={deckStyle.cardSuitIcon} src={
                            (card.suit === 'spades' ? spades :
                              (card.suit === 'diamonds' ? diamonds :
                                (card.suit === 'clubs' ? clubs :
                                  (card.suit === 'hearts' ? hearts : '')))) } alt=""/>
                        </div>
                        <span>{card.value}</span>
                      </div>
                      <div className={deckStyle.middlePart}>
                        <div className={`${deckStyle.cardValue}`}>

                          {
                            (card.value === 'K'  ?
                              <Fragment>
                                <div className={`col-12` }>
                                  <img src={crown} alt=""/>
                                </div>
                              </Fragment> : (
                                card.value === 'Q' ?
                                  <Fragment>
                                    <div className={`col-12` }>
                                      <img className={deckStyle.queenCardSuitIcon} src={queens_crown} alt=""/>
                                    </div>
                                  </Fragment>
                                 :
                                    ( card.value === 'J' ?
                                        <Fragment>
                                          <div className={`col-12` }>
                                            <img className={deckStyle.queenCardSuitIcon} src={swords} alt=""/>
                                          </div>
                                        </Fragment>:
                                        <div className={`col-12` }>
                                          <span className={deckStyle.simpleCards}>{card.value}</span>
                                        </div>
                                    )
                              ))
                          }
                        </div>
                      </div>
                      <div className={deckStyle.bottomPart}>
                        <div className={deckStyle.cardSuitBottomRight}>
                          <img className={deckStyle.cardSuitIcon} src={
                            (card.suit === 'spades' ? spades :
                              (card.suit === 'diamonds' ? diamonds :
                                (card.suit === 'clubs' ? clubs :
                                  (card.suit === 'hearts' ? hearts : '')))) } alt=""/>
                        </div>
                        <span>{card.value}</span>
                      </div>
                    </div>
                  </div>
                </div>

            )
          })}
          </div>
        </Fragment>
      )
    }
  };

  render(){
    return(
      <Fragment>
        {this.playerCards()}
      </Fragment>
    )
  }
}